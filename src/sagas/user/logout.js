import sdk from '../../utils/getSdk';
import userLogout from 'utils/userLogout';

export default function* tryUserLogout({data}) {
  yield sdk.userLogout({authToken: localStorage.getItem('authToken')});
  userLogout();
}
