import {actionTypes as userActions} from 'features/user';
import {put} from 'redux-saga/effects';

export default function* afterLoginSuccess({data}) {
  yield put({type: userActions.GET_TRY, data: {}});
}
