import {put, takeLatest, takeEvery} from 'redux-saga/effects';
import {actionTypes as userActions} from 'features/user';
import snackbar from 'utils/snackbar';

import tryUserLogin from './user/login';
import tryUserLogout from './user/logout';
import tryUserGet from './user/get';
import afterLoginSuccess from './user/afterLoginSuccess';

export default function* rootSaga() {
  yield takeLatest(userActions.LOGIN_TRY, tryUserLogin);
  yield takeEvery(userActions.LOGOUT_TRY, tryUserLogout);
  yield takeLatest(userActions.LOGIN_SUCCESS, afterLoginSuccess);
  yield takeLatest(userActions.GET_TRY, tryUserGet);

  if (typeof localStorage !== 'undefined') {
    const authToken = localStorage.getItem('authToken');
    if (authToken) {
      snackbar({message: 'Welcome back'});
      yield put({type: userActions.LOGIN_SUCCESS, data: {authToken}});
    }
  }
}
