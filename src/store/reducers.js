import {combineReducers} from 'redux';
import userReducer, {NAME as userName} from 'features/user';
import {connectRouter} from 'connected-react-router';

export default (history) => combineReducers({
  [userName]: userReducer,
  router: connectRouter(history),
});
