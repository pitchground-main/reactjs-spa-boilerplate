export default function userLogout() {
  if (typeof localStorage !== 'undefined') {
    localStorage.removeItem('authToken');
    window.location = '/';
  }
}
