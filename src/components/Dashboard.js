import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {selector as userSelector} from 'features/user';
import Layout from 'components/Layout';

function Dashboard({user}) {
  return (
    <Layout>
      <div style={{maxWidth: 1000, margin: '24px auto', padding: '0 6px'}}>
        Hi from Dashboard
        {JSON.stringify(user)}
      </div>
    </Layout>
  );
}

Dashboard.propTypes = {
  user: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
  return {
    ...userSelector(state),
  };
}

export default connect(
  mapStateToProps,
)(Dashboard);
