import React from 'react';
import PropTypes from 'prop-types';
import {Layout} from 'antd';
import NavigationTop from 'components/NavigationTop';

const {Footer, Content} = Layout;
const moment = require('moment');

function AppLayout({children}) {
  return (
    <Layout>
      <NavigationTop/>
      <Content>
        {children}
      </Content>
      <Footer>
        <div style={{textAlign: 'center', lineHeight: '32px'}}>
          &copy; {moment().format('YYYY')}
        </div>
      </Footer>
    </Layout>
  );
}

AppLayout.propTypes = {
  children: PropTypes.node.isRequired,
};

export default AppLayout;
